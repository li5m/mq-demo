package com.ming.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class ProducersReply {

    @JmsListener(destination = "tempqueue")
    public void receiveTopic(String text) {
        System.out.println(this.getClass().getName() + " 收到的报文为:" + text);
    }
}
