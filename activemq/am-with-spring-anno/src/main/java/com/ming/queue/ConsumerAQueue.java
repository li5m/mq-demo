package com.ming.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class ConsumerAQueue {

    @JmsListener(destination = "spring.queue")
    @SendTo("tempqueue")
    public String receiveTopic(String text) {
        System.out.println(this.getClass().getName() + " 收到的报文为:" + text);
        return "receiveTopic...收到了消息：" + text;
    }
}
