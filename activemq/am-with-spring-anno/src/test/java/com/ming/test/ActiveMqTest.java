package com.ming.test;

import com.ming.ActiveMqConfiguration;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.locks.LockSupport;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ActiveMqConfiguration.class)
public class ActiveMqTest {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Test
    public void test() throws Exception {
        ActiveMQQueue activeMQQueue = new ActiveMQQueue("spring.queue");
        System.out.println(this.getClass().getName() + "发送消息：hello");
        jmsTemplate.convertAndSend(activeMQQueue, "hello");
        LockSupport.park();
    }
}
