package cn.enjoyedu.testrabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 *@author Mark老师   享学课堂 https://enjoy.ke.qq.com
 *往期视频咨询芊芊老师  QQ：2130753077  VIP课程咨询 依娜老师  QQ：2470523467
 *类说明：direct类型交换器的生产者
 */
public class TestProducer {

    public final static String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args)
            throws IOException, TimeoutException {
        /* 创建连接,连接到RabbitMQ*/
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.99.100");
        connectionFactory.setPort(5670);

        Connection connection = connectionFactory.newConnection();

        /*创建信道*/
        Channel channel = connection.createChannel();
        /*创建交换器*/
        channel.exchangeDeclare(EXCHANGE_NAME,"direct",true);
        //channel.exchangeDeclare(EXCHANGE_NAME,BuiltinExchangeType.DIRECT);
        channel.confirmSelect();
        /*日志消息级别，作为路由键使用*/
//        String[] serverities = {"error","info","warning"};
        for(int i=0;i<100000;i++){
            String severity = "error";
            String msg = "Hellol,RabbitMq"+(i+1);
            /*发布消息，需要参数：交换器，路由键，其中以日志消息级别为路由键*/
            channel.basicPublish(EXCHANGE_NAME,severity, /*MessageProperties.PERSISTENT_TEXT_PLAIN*/null,
                    msg.getBytes());
            System.out.println("Sent "+severity+":"+msg);
            try {
                channel.waitForConfirms();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("发送完成！");
        channel.close();
        connection.close();

    }

}
